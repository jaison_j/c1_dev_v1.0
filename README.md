# C1_Node_dev_v1.0
Ultra Low Power C1_Node_dev_v1 Board repository contains hardware sources and documentation.

# Getting Started
- Make sure that you have a C1_Node_revb_v1.0.
- Install STM32CubeIDE.
- Select board : B-L072Z-LRWAN1


# Prerequisites
STM32CubeIDE [Tested]

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments

# Changelog
